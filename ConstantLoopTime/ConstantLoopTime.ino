/*
ConstantLoopTime.ino
*/

uint32_t lastTime = 0;
uint32_t startTime;
uint32_t loopTime = 0;
uint16_t buffer = 1000; // extra time buffer allowed per loop (microseconds)

void setup() {
	Serial.begin(9600);
	lastTime = micros();
}

void loop() {
	// read current time
	startTime = micros();
	// read sensor here

	// print info
	Serial.print(lastTime);
	Serial.print("\t");
	Serial.print("blah blah blah"); // print sensor data
	Serial.print("\t");
	Serial.println(loopTime);

	// wait until loopTime is reached
	uint32_t now = 0;
	while ((now - lastTime) <= loopTime + buffer) {
		now = micros();
	}

	// update loopTime, if necessary
	loopTime = max(loopTime, now - lastTime);

	// store last time
	lastTime = micros();
}