// This is initial spacing between the input and the upper and lower bounds
// The distance from upper to lower bound will be 2*hysteresis+1
const uint8_t hysteresis = 10;

/*
Applies hysteresis
First time, setup bounds around the input
If within bounds, don't change output (output will always be the center of the bounds)
Once those bounds are exceeded, shift bounds resulting in new output
In out
      	[  *  ]		Start (bounds start centered about input)
>     	[   * ]
>     	[    *]
>   > 	 [    *]	(exceeded upper bound, bounds shifted up so input is at the upper bound)
>   > 	  [    *]	(bounds shifted again so input is at upper bound)
<     	  [   * ]
>     	  [    *]
<     	  [   * ]
<     	  [  *  ]
<     	  [ *   ]
<     	  [*    ]
<   < 	 [*    ]	(bounds shifted down because lower bound is exceeded)
<   < 	[*    ]	(bounds shifted down because lower bound is exceeded)
>     	[ *   ]
>     	[  *  ]
<     	[ *   ]
>     	[  *  ]
>     	[   * ]
>     	[    *]
>   > 	 [    *]	(bounds shifted again so input is at upper bound)
>   > 	  [    *]	(bounds shifted again so input is at upper bound)
*/
uint16_t applyHysteresis(uint16_t input)
{
	// This initialization only happens the first time this is called.
	// After that, the function remembers their previous values
	static uint16_t upperBound = input + hysteresis;
	static uint16_t lowerBound = input - hysteresis;

	if (input > upperBound)
	{
		// Shift bounds up
		upperBound = input;
		lowerBound = input - 2 * hysteresis;
	}
	else if (input < lowerBound)
	{
		// Shift bounds down
		upperBound = input + 2 * hysteresis;
		lowerBound = input;
	}

	Serial.print(" ub:\t");
	Serial.print(upperBound);
	Serial.print(" lb:\t");
	Serial.print(lowerBound);
	Serial.print("\t");

	// Output is the center of the bounds
	return lowerBound + hysteresis;
}

void setup() 
{
	pinMode(A0, INPUT);
	Serial.begin(9600);
}


void loop()
{
	uint16_t input = analogRead(A1);
	Serial.print("input:\t");
	Serial.print(input);
	Serial.print("\t" );
	Serial.println(applyHysteresis(input));
}